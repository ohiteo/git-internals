package core;

import java.io.*;
import java.util.zip.*;
import java.nio.file.*;



public class GitCommitObject{
	
	String hash, treeHash, parentHash, author;

	public GitCommitObject(String repopath, String masterCommitHash){
		this.hash=masterCommitHash;
		String filepath=repopath+"/objects/"+masterCommitHash.substring(0,2)+"/"+masterCommitHash.substring(2);

		try {
			Path path=Paths.get(filepath);
			byte[] data = Files.readAllBytes(path);
			
			Inflater decompresser=new Inflater();
			decompresser.setInput(data,0,data.length);
			byte[] result=new byte[1000];
			int resultlen=decompresser.inflate(result);
			decompresser.end();

			String datipuliti=new String(result, 0, resultlen, "UTF-8");
			System.out.println(datipuliti);
			this.treeHash=(datipuliti.split("\n")[0]).split(" ")[2];

			this.parentHash=(datipuliti.split("\n")[1]).split(" ")[1];

			this.author=(((datipuliti.split("\n")[2]).split("author ")[1]).split(">")[0])+">";

		}
		catch(Exception e){ System.out.println(e.toString());	}
	}

	public String getHash(){
		return hash;
	}
	public String getTreeHash(){
		return treeHash;
	}
	public String getParentHash(){
		return parentHash;
	}
	public String getAuthor(){
		return author;
	}
}	