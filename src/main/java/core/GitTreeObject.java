package core;

import java.io.*;
import java.util.zip.*;
import java.nio.file.*;
import java.util.ArrayList;

public class GitTreeObject{
	String type, repopath, stringacontenuto;
	ArrayList <String> entrypaths=new ArrayList<String>();
	ArrayList <String> entrypathshash=new ArrayList<String>();
	

	public GitTreeObject(String repopath, String masterTreeHash){
		this.repopath=repopath;
		String treepath=repopath+"/objects/"+masterTreeHash.substring(0,2)+"/"+masterTreeHash.substring(2);
			
		try{
			FileInputStream fis = new FileInputStream(treepath);
			InflaterInputStream inStream = new InflaterInputStream(fis);
			int i;
			type="";
			while((i = inStream.read()) != 0x20){ //Scorro fino a che non incontro uno 0
				type+= (char) i; //Qui calcoliamo il Type
			}
			//Content data
			while((i = inStream.read()) != -1){
				while((i = inStream.read()) != 0x20){ //Scorriamo fino a che non troviamo uno spazio
			    	//Qui troviamo il mode del file.
				}
				String filename="";
				while((i = inStream.read()) != 0){
					filename += (char) i;
				}
				//System.out.println("FileName:"+filename);
				entrypaths.add(filename); //PATH FILE

				//Hash: 20 byte long
				String hash = "";
				for(int count = 0; count < 20 ; count++){
					i = inStream.read();
					if((int) i<10) hash+=0;
					hash += Integer.toHexString(i);
				}
				//System.out.println("Hash:"+hash);
				entrypathshash.add(hash); //HASH FILE			
			}
		} catch(Exception e){System.out.println(e.toString());	}
	}

	public String getType(){
		return type;
	}

	public ArrayList<String> getEntryPaths(){
		System.out.println(entrypaths);
		return entrypaths;
	}
		
	public Object getEntry(String name){
		int i=0;
		for(String path: entrypaths){
			if(path.equals(name)){
				String hash=entrypathshash.get(i);
				String letto=decomprimi(repopath+"/objects/"+hash.substring(0,2)+"/"+hash.substring(2));
				String type=letto.substring(0,letto.indexOf(" "));
				Object o=null;
				switch(type){
					case "blob":
						o=new GitBlobObject(repopath, hash);
						break;
					case "commit":
						o=new GitCommitObject(repopath, hash);
						break;
					case "tree":
						o=new GitTreeObject(repopath, hash);
						break;
					case "repository":
						o=new GitRepository(repopath);
						break;
					default:
						break;
				}
				return o;
			}
			i++;
		}	
		System.out.println("Il nome inserito non corrisponde a nessun file");
		
		return null;
	}
	private String decomprimi(String filepath){
		try {
			Path path=Paths.get(filepath);
			byte[] data = Files.readAllBytes(path);
			Inflater decompresser=new Inflater();
			decompresser.setInput(data,0,data.length);
			byte[] result=new byte[1000];
			int resultlen=decompresser.inflate(result);
			decompresser.end();
			return new String(result, 0, resultlen, "UTF-8");
		} catch(Exception e) {System.out.println(e.toString());}

		return null;
	}
}	