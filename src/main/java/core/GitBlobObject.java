package core;

import java.io.*;
import java.util.zip.*;
import java.nio.file.*;



public class GitBlobObject{
	String filepath, type, content, hash;

	public GitBlobObject(String repo, String hash){
		this.filepath=repo+"/objects/"+hash.substring(0,2)+"/"+hash.substring(2);
		this.hash=hash;
		try {
			Path path=Paths.get(filepath);
			byte[] data = Files.readAllBytes(path);
			
			Inflater decompresser=new Inflater();
			decompresser.setInput(data,0,data.length);
			byte[] result=new byte[100];
			int resultlen=decompresser.inflate(result);
			decompresser.end();

			String datipuliti=new String(result, 0, resultlen, "UTF-8");

			this.type=datipuliti.split(" ")[0];
			this.content=datipuliti.split("\0")[1];
		}
		catch(Exception e){
			System.out.println(e.toString());
		}
		
	}
	public String getHash(){
		return hash;
	}

	public String getType(){
		return this.type;
	}
	public String getContent(){
		return this.content;
	}
}	