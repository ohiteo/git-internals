package core;

import java.io.*;

public class GitRepository{
	private String repository;

	public GitRepository(String repository){
		this.repository=repository;
	}
	
	public String getHeadRef(){
		try{
			BufferedReader in= new BufferedReader(new FileReader(repository+"/HEAD")); 
			return in.readLine().substring(5);
		}
		catch(IOException e){ System.out.println(e.toString());}
		return null;
	}
	
	public String getRefHash(String s){
		try{
			BufferedReader in= new BufferedReader(new FileReader(repository+"/"+s)); 
			return in.readLine();
		}
		catch(IOException e) {System.out.println(e.toString());}
		return null;

	}

}