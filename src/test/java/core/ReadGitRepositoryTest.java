package core;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;


public class ReadGitRepositoryTest {

	private  String repoPath;
	private  GitRepository repository;
	
	@Before
	public void setup() {
		repoPath = new String("sample_repos/sample01");
		repository = new GitRepository(repoPath);
	}
	
	@Test
	public void shouldFindHead() throws Exception {
		assertThat(repository.getHeadRef()).isEqualTo("refs/heads/master");
	}
	
	//TODO: trovare da shell l'hash del commit relativo al master del repository in sample_repos/sample01
	private static String masterCommitHash = "1e7c193371b42215bc9ba653c48f58ebbb1a7aae"; 

	@Test
	public void shouldFindHash() throws Exception {
		assertThat(repository.getRefHash("refs/heads/master")).isEqualTo(masterCommitHash);
	}

	@Test
	public void shouldGetBlobContent() throws Exception {
		GitBlobObject blobObject = new GitBlobObject(repoPath,"4452771b4a695592a82313e3253f5e073e6ead8c");
		assertThat(blobObject.getType()).isEqualTo("blob");
		assertThat(blobObject.getContent()).isEqualTo("REPO DI PROVA\n=============\n\nSemplice repository Git di prova\n");		
	}

	//TODO: trovare da shell l'hash del tree relativo al master del repository in sample_repos/sample01
	private static String masterTreeHash = "b97a44efe72ac12363fd19654c348e81e22a0266";
	
	@Test
	public void shouldGetCommitObject() throws Exception {
		GitCommitObject commit = new GitCommitObject(repoPath, masterCommitHash);
		assertThat(commit.getHash()).isEqualTo(masterCommitHash);
		assertThat(commit.getTreeHash()).isEqualTo(masterTreeHash);
		assertThat(commit.getParentHash()).isEqualTo("c4c62d173aa9ebd5497f10db6b9826539e60108e");
		assertThat(commit.getAuthor()).isEqualTo("Carlo Bellettini <carlo.bellettini@unimi.it>");
	}

	@Test
	public void shouldGetTreeObject() throws Exception {
		GitTreeObject treeObject = new GitTreeObject(repoPath, masterTreeHash);
		assertThat(treeObject.getEntryPaths()).containsExactlyInAnyOrder("file4","file2", "dir1", "file1","README.md");
		assertThat(treeObject.getEntry("README.md")).isExactlyInstanceOf(GitBlobObject.class);
		assertThat(((GitBlobObject)treeObject.getEntry("README.md")).getHash()).isEqualTo("4452771b4a695592a82313e3253f5e073e6ead8c");
		assertThat(treeObject.getEntry("dir1")).isExactlyInstanceOf(GitTreeObject.class);
		assertThat(((GitTreeObject)treeObject.getEntry("dir1")).getType()).isEqualTo("tree");
	}
}
